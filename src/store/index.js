import { createStore } from "vuex";

import defaultUsers from "../assets/users.json";
import defaultTasks from "../assets/tasks.json";

const localTasks = localStorage.getItem("test_tasks");

const resTasks = localTasks ? JSON.parse(localTasks) : defaultTasks;

export default createStore({
  state: {
    users: [...defaultUsers],
    tasks: [...resTasks],
  },
  mutations: {
    add(state, task) {
      state.tasks = [task, ...state.tasks];
    },
    update(state, { task, id: taskId }) {
      let newTasks = [...state.tasks];
      const oldTaskIndex = newTasks.findIndex(({ id }) => id === taskId);
      const oldTask = newTasks[oldTaskIndex];
      if (task.status !== oldTask.status) {
        newTasks.splice(oldTaskIndex, 1);
        newTasks.unshift(task);
      } else newTasks.splice(oldTaskIndex, 1, task);
      state.tasks = [...newTasks];
    },
    remove(state, taskId) {
      state.tasks = state.tasks.filter(({ id }) => id !== taskId);
    },
    move(state, { id: adjId, dragId, status, isTop }) {
      const newTasks = [...state.tasks];

      const dragIndex = newTasks.findIndex(({ id }) => id === dragId);
      const dragTask = newTasks[dragIndex];

      newTasks.splice(dragIndex, 1);

      const movedTask = { ...dragTask, status };

      if (adjId) {
        const adjIndex = newTasks.findIndex(({ id }) => id === adjId);
        newTasks.splice(adjIndex + Number(!isTop), 0, movedTask);
      } else newTasks.push(movedTask);
      state.tasks = [...newTasks];
    },
  },
  modules: {},
});

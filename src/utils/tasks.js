export const statuses = {
  toDo: "To Do",
  inProgress: "In Progress",
  test: "Testing",
  done: "Done",
};

export const initialTask = {
  name: "",
  deadline: null,
  description: "",
  userId: null,
  status: statuses.toDo,
};

export const checkHover = (s1, e1, s2, e2) => {
  const left = Math.max(s1, s2);
  const right = Math.min(e1, e2);

  const diff = right - left;

  return {
    diff,
    value: diff > 0,
    isTop: s1 > s2,
  };
};

export const initialHoveredData = {
  id: null,
  isTop: null,
  status: null,
};

export const getHoveredData = (
  refs,
  dragTaskId,
  statusesWithTasks,
  statusesNames
) => {
  let hoveredTaskData = { ...initialHoveredData };
  const hoveredStatuses = [];
  const hoveredTasks = [];
  let nearestTask = null;

  const {
    left: dragLeft,
    right: dragRight,
    top: dragTop,
    bottom: dragBottom,
  } = refs[dragTaskId].getBoundingClientRect();

  const dragCenterY = dragTop - (dragTop - dragBottom) / 2;

  statusesNames.forEach((statusName) => {
    const {
      left: statusLeft,
      right: statusRight,
      top: statusTop,
      bottom: statusBottom,
    } = refs[statusName].getBoundingClientRect();
    const overlayX = checkHover(statusLeft, statusRight, dragLeft, dragRight);
    const overlayY = checkHover(statusTop, statusBottom, dragTop, dragBottom);

    if (overlayX.value && overlayY.value) {
      hoveredStatuses.push({
        name: statusName,
        dist: overlayX.diff * overlayX.diff,
      });
    }
  });

  const hoveredStatusName = hoveredStatuses.length
    ? hoveredStatuses.reduce((max, obj) => (max.dist > obj.dist ? max : obj))
        ?.name
    : null;
  if (hoveredStatusName) {
    const hoveredStatus = statusesWithTasks.find(
      ({ name }) => name === hoveredStatusName
    );

    hoveredStatus.tasks.forEach(({ id }) => {
      if (id !== dragTaskId) {
        const { left, right, top, bottom } = refs[id].getBoundingClientRect();

        const taskCenterY = top - (top - bottom) / 2;
        const dist = Math.abs(dragCenterY - taskCenterY);

        if (!nearestTask || nearestTask.dist > dist)
          nearestTask = { id, dist, top, bottom };

        const overlayX = checkHover(left, right, dragLeft, dragRight);
        const overlayY = checkHover(top, bottom, dragTop, dragBottom);

        if (overlayX.value && overlayY.value) {
          hoveredTasks.push({
            id,
            dist: overlayY.diff,
            isTop: overlayY.isTop,
          });
        }
      }
    });

    hoveredTaskData.status = hoveredStatusName;

    if (hoveredTasks.length) {
      hoveredTaskData = {
        ...hoveredTaskData,
        id: hoveredTasks[0].id,
        isTop: hoveredTasks[0].isTop,
      };
    } else if (hoveredStatus.tasks.length && nearestTask) {
      const isTop =
        Math.abs(dragCenterY - nearestTask.top) <
        Math.abs(dragCenterY - nearestTask.bottom);

      hoveredTaskData = {
        ...hoveredTaskData,
        id: nearestTask.id,
        isTop,
      };
    }
  }
  return hoveredTaskData;
};
